#include "utils.h"

#include <fstream>

vector<string> Utils::split(const string &s, char delim)
{
    vector<string> elems;

    stringstream ss;
    ss.str(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }

    return elems;
}

string Utils::vectorToString(const vector<string> &v, char delim)
{
    std::stringstream ss;

    for(size_t i = 0; i < v.size(); ++i)
    {
        if(i != 0)
            ss << delim;
        ss << v[i];
    }

    return ss.str();
}

streampos Utils::fileZise(const string &filePath)
{
    streampos fsize = 0;
    ifstream file(filePath, ios::binary );

    fsize = file.tellg();
    file.seekg(0, ios::end);
    fsize = file.tellg() - fsize;
    file.close();

    return fsize;
}
