#include "app.h"

int main(int argc, char *argv[])
{
    App app;
    if (!app.run())
        app.printLastError();

    return 0;
}
