﻿#ifndef APP_H
#define APP_H

#include <string>
#include <chrono>

using namespace std;

class App
{
public:
    App();

    //! Выполнение обработтки файла
    bool run();

    string lastError();
    void printLastError();

private:
    //! Путь к исходному файлу
    bool typeInputFileName();
    //! Путь к выходному файлу
    bool typeOutputFileName();

    //! Обработка файла
    bool process();

    //! Вывод результата в JSON
    void writeJsonFile();

private:
    string _inputFilePath; // Путь к исходному файлу
    string _outputFilePath; // Путь к выходному файлу

    string _lastError;

    chrono::time_point<chrono::system_clock> _start; // Старт выполнения обработки
    chrono::time_point<chrono::system_clock> _end; // Конец выполнения обработки
    int _elapsedSeconds; // Время в секундах, затраченное на обработку
};


#endif // APP_H
