#include "app.h"
#include "utils.h"

#include <json/value.h>

#include <iostream>
#include <fstream>
#include <ctime>
#include <iomanip>
#include <bitset>
#include <vector>

App::App()
{

}

bool App::run()
{
    _start = chrono::system_clock::now();

    if (!typeInputFileName()) {
        _lastError = "Error! Empty input file name.";
        return false;
    }

    if (!typeOutputFileName()) {
        _lastError = "Error! Empty output file name.";
        return false;
    }

    chrono::time_point<std::chrono::system_clock> processStart = chrono::system_clock::now();

    if (!process())
        return false;

    chrono::time_point<std::chrono::system_clock> processEnd = chrono::system_clock::now();

    _elapsedSeconds = chrono::duration_cast<chrono::seconds>(processEnd - processStart).count();
    _end = std::chrono::system_clock::now();

    writeJsonFile();

    return true;
}

string App::lastError()
{
    return _lastError;
}

void App::printLastError()
{
    cout << "\n" << _lastError << "\n" << endl;
}

bool App::typeInputFileName()
{
    cout << "Please, type input file path: " << endl;
    getline(cin, _inputFilePath);

    if (!_inputFilePath.empty())
        cout << "Input file path: " << _inputFilePath << endl;

    return !_inputFilePath.empty();
}

bool App::typeOutputFileName()
{
    vector<string> pathVector = Utils::split(_inputFilePath, '/');
    pathVector[pathVector.size() - 1] = string("output_") + pathVector[pathVector.size() - 1];
    string defaultOutputFilePath = Utils::vectorToString(pathVector, '/');

    cout << "Please, type output file path (" << defaultOutputFilePath << "): " << endl;

    getline(cin, _outputFilePath);

    if (_outputFilePath.empty())
        _outputFilePath = defaultOutputFilePath;

    if (!_outputFilePath.empty())
        std::cout << "Output file path: " << _outputFilePath << std::endl;

    return !_outputFilePath.empty();
}

bool App::process()
{
    ifstream input(_inputFilePath, std::ios::binary);

    if (!input.is_open()) {
        _lastError = "Error! Input file does not exist.";
        return false;
    }

    // Буфер битов
    vector<bool> bits;

    ofstream reportBinOfs(_outputFilePath, ofstream::out);

    // Обрабатывается кадр
    bool frame = false;
    // Буфер, в котором находится содержимое кадра
    vector<bool> frameData;

    // Предыдущий прочитанный байт из буфера
    bitset<8> lastB1;

    int repeatBytes = 0;

    // Читаем 100 кб
    for (int i = 0; i < 100000; ++i) {
        char c;
        input.get(c);
        bitset<8> b(c);

        // Записываем биты в вектор
        for (int j = 7; j >= 0; --j)
            bits.push_back(b[j]);


        //Флаг FD  Адрес  Управляющее поле  Информационное поле	       FCS	   Флаг FD
        //8 бит	   8 бит  8 бит	            0 или более бит, кратно 8  16 бит  8 бит

        // Обрабатываем вектор
        // 01110101111110
        // 01110101 -- не начало кадра, удаляем первый бит
        //  11101011
        //   11010111
        //    10101111
        //     01011111
        //      10111111
        //       01111110 -- Флаг

        // Сначала ищем флаг
        // Как только флаг найден, ищем следующий флаг.
        // Все что находится между флагами записывается во временный буфер
        // Из временного буфера извлекаются адрес, управляющее поле и CRC
        // Во временном буфере остается только информационное поле из
        // которого удаляется 0 после 5 идущих 1

        // 01111110 -- repeatBytes = 0
        // 01111110 01111110 -- repeatBytes = 1
        // 01111110 01111110 01111110 -- repeatBytes = 2 >> 01111110 01111110 -- repeatBytes = 1
        //

        // Иногда флаг конца одного кадра может (но не обязательно) быть начальным флагом следующего кадра.

        while (bits.size() >= 8) {
            bitset<8> b1;

            for (int j = 0; j < b.size(); ++j)
                b1[7-j] = bits[j];

            if (lastB1 == 0x7E && b1 != 0x7E) {
                // Если это не 2 друг за другом флага
                if (repeatBytes != 1) {
                    cout << "Start frame" << endl;
                    frame = true;
                }
            } else if (lastB1 == 0x7E && b1 == 0x7E) {
                // Если предыдущий и текущий кадр повторяются, то увиличиваем счетчик
                ++repeatBytes;

                // Если счетчик больше 1, значит это межкадровое временное заполнение
                // Удаляем предыдущий байт
                if (repeatBytes > 1) {
                    for (int j = 0; j < 7; ++j)
                        bits.erase(bits.begin());
                    repeatBytes = 1;
                }

                // Запоминаем текущий байт
                lastB1 = b1;

                continue;
            }

            // Запоминаем текущий байт
            lastB1 = b1;

            if (frame) {
                // Если это флаг, конец кадра
                if (b1 == 0x7E) {
                    cout << "End frame" << endl;
                    frame = false;

                    if (frameData.size() >= 32) {
                        // Обрабатываем содержимое кадра
                        // Первые 8 бит - адрес

                        bitset<8> address;
                        for (int l = 7; l >= 0; --l) {
                            address[l] = frameData.front();
                            frameData.erase(frameData.begin());
                        }

                        reportBinOfs << char(address.to_ulong());

                        // Управляюще поле - 8 бит
                        bitset<8> control;
                        for (int l = 7; l >= 0; --l) {
                            control[l] = frameData.front();
                            frameData.erase(frameData.begin());
                        }

                        reportBinOfs << char(control.to_ulong());

                        // Последние 16 бит - CRC
                        bitset<8> crc1;
                        bitset<8> crc2;

                        for (int l = 0; l <= 7; ++l) {
                            crc2[l] = frameData.back();
                            frameData.erase(frameData.end());
                        }

                        for (int l = 0; l <= 7; ++l) {
                            crc1[l] = frameData.back();
                            frameData.erase(frameData.end());
                        }

                        // Удаление bit staffing
                        for (long int l = 0; l < frameData.size();) {
                            if ((l+6) >= frameData.size() )
                                break;

                            if (frameData[l] == true &&
                                    frameData[l + 1] == true &&
                                    frameData[l + 2] == true &&
                                    frameData[l + 4] == true &&
                                    frameData[l + 5] == true) {
                                frameData.erase(frameData.begin() + l + 6);
                                l += 6;
                                continue;
                            } else
                                ++l;
                        }

                        bitset<8> buffBitset1;
                        int buffBitsetIndex1 = 7;

                        while (!frameData.empty()) {
                            if (buffBitsetIndex1 == -1) {
                                reportBinOfs << char(buffBitset1.to_ulong());
                                buffBitsetIndex1 = 7;
                            }

                            buffBitset1[buffBitsetIndex1] = frameData.front();
                            --buffBitsetIndex1;
                            frameData.erase(frameData.begin());
                        }


                        reportBinOfs << char(crc2.to_ulong());
                        reportBinOfs << char(crc1.to_ulong());

                    }

                    frameData.clear();

                    // Удаляем из вектора первые восемь битов
                    for (int l = 0; l < b1.size(); ++l)
                        if (bits.size() > 0)
                            bits.erase(bits.begin());

                    repeatBytes = 0;

                    reportBinOfs << char(b1.to_ulong());

                } else {
                    // Если это кадр, то записываем его содержимое в вектор, до тех пор пока не будет очередной флаг
                    if (bits.size() > 0) {
                        frameData.push_back(bits.front());
                        bits.erase(bits.begin());
                    }
                }

            } else {
                // Если это флаг, начало кадра
                if (b1 == 0x7E) {
                    cout << "Start frame" << endl;
                    frame = true;

                    // Удаляем из вектора первые восемь битов
                    for (int l = 0; l < b1.size(); ++l)
                        if (bits.size() > 0)
                            bits.erase(bits.begin());

                    reportBinOfs << char(b1.to_ulong());

                } else {
                    bits.erase(bits.begin());
                }
            }
        }
    }

    return true;
}

void App::writeJsonFile()
{
    time_t start_t = chrono::system_clock::to_time_t(_start);
    time_t end_t = chrono::system_clock::to_time_t(_end);

    Json::Value root;

    root["encoding"] = "UTF-8";
    root["input-file-path"] = _inputFilePath.c_str();
    root["input-file-size "] = static_cast<int>(Utils::fileZise(_inputFilePath));
    root["output-file-path"] = _outputFilePath.c_str();
    root["output-file-size"] = static_cast<int>(Utils::fileZise(_outputFilePath));
    root["start"] = ctime(&start_t);
    root["end"] = ctime(&end_t);
    root["elapsed-seconds"] = _elapsedSeconds;

    std::ofstream ofs ("report.json", std::ofstream::out);
    ofs << root.toStyledString();
}
