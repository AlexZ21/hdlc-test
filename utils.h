#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <sstream>
#include <vector>

using namespace std;

class Utils
{
public:
    static vector<string> split(const string &s, char delim);

    static string vectorToString(const vector<string> &v, char delim);

    static streampos fileZise(const string &filePath);

private:
    Utils(){}
};

#endif // UTILS_H
